module.exports = {
  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!src/**/*.d.ts'],
  coverageThreshold: {
    global: {
      'branches': 0,
      'functions': 1,
      'lines': 1,
      'statements': 1
    }
  },
  coverageDirectory: '<rootDir>/coverage',
  maxConcurrency: 5,
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testEnvironment: "jest-environment-node",
  moduleNameMapper: {
    "\\.(css|scss|sass)$": "identity-obj-proxy"
  },
  testPathIgnorePatterns: ['./public/', './node_modules/', './config/', './coverage/'],
  setupFilesAfterEnv: ['<rootDir>/src/setupTests.js'],
  testMatch: ['<rootDir>/src/**/*.test.js'],
  modulePaths: ['<rootDir>/src'],
  verbose: true,
  restoreMocks: true,
  coverageReporters: ['lcov'],
};
