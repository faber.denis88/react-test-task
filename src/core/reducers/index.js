import {HOME_PAGE_KEY, homeReducer} from '../../pages/home'
const reducers = {
    [HOME_PAGE_KEY]: homeReducer
};

export const getReducers = () => ({...reducers});
