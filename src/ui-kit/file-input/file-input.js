import PropTypes from "prop-types";
import {FILE_INPUT_EXTENSIONS} from "./constants";

export const FileInput = ({accept, onChange}) => {
    return <input type="file" accept={accept} onChange={onChange}/>
}

FileInput.propTypes = {
    onChange: PropTypes.func,
    accept: PropTypes.arrayOf(PropTypes.oneOf(Object.values(FILE_INPUT_EXTENSIONS)))
}