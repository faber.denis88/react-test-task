export const FILE_INPUT_EXTENSIONS = {
    video: 'video/*',
    audio: 'audio/*',
    mediaType: 'media_type'
}