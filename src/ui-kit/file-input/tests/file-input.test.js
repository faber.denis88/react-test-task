import {FileInput} from "../file-input";

import {shallow} from 'enzyme';
import {FILE_INPUT_EXTENSIONS} from "../constants";

describe('FileInput',() => {
    it('should render successfully', function () {
        const wrap = shallow(<FileInput onChange={jest.fn()} accept={[FILE_INPUT_EXTENSIONS.video]}/>);

        expect(wrap).toMatchSnapshot();
    });
    describe('behavior', function () {
        describe('when click on input', function () {
            it('should call onClick', function () {
                const onChange = jest.fn();
                const wrap = shallow(<FileInput onChange={onChange} accept={[FILE_INPUT_EXTENSIONS.video]}/>);

                wrap.find('input').simulate('change', {
                    target: {
                        files: [
                            'dummyValue.something'
                        ]
                    }
                });
                expect(onChange).toHaveBeenCalled();
            });
        });
    });
})