import {Button} from "../button";
import {shallow} from 'enzyme';
import {BUTTON_THEME} from "../constants";

describe('Button',() => {
    it('should render successfully', function () {
        const wrap = shallow(<Button onClick={jest.fn()} name="Button" theme={BUTTON_THEME.red} />);

        expect(wrap).toMatchSnapshot();
    });
    describe('behavior', function () {
        describe('when click on button', function () {
            it('should call onClick', function () {
                const onClick = jest.fn();
                const wrap = shallow(<Button onClick={onClick} name="Button" />);
                wrap.find('button').simulate('click');
                expect(onClick).toHaveBeenCalled();
            });
        });
    });
})