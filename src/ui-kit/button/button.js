import PropTypes from 'prop-types';
import classNames from 'classnames';
import {BUTTON_THEME} from './constants';
import './button.scss';

export const Button = ({name, onClick, theme}) => {
    const classes = classNames(
        `button`,
        {
            'button--green': theme === BUTTON_THEME.green,
            'button--red': theme === BUTTON_THEME.red,
        }
    );
    return <button className={classes} onClick={onClick}>{name}</button>

}
Button.propTypes = {
    name: PropTypes.string,
    onClick: PropTypes.func,
    theme: PropTypes.oneOf(Object.values(BUTTON_THEME))
}