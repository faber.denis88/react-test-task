import {shallow} from 'enzyme';
import {VideoPlayer} from "../video-player";

describe('VideoPlayer',() => {
    it('should render successfully', function () {
        const wrap = shallow(<VideoPlayer renderControls={jest.fn()} url="some url" />);

        expect(wrap).toMatchSnapshot();
    });
})