import PropTypes from 'prop-types';
import React, {useRef} from "react";

import './video-player.scss';

export const VideoPlayer = ({url, renderControls = ()=>{}}) => {
    const videoRef = useRef();
    const handlePlay = () => {
        videoRef.current.play();
    }
    const handleStop = () => {
        videoRef.current.pause();
    }
    const handleClick = () => {
        if (videoRef.current.paused || videoRef.current.ended) {
            videoRef.current.play();
        } else {
            videoRef.current.pause();
        }
    }
    return <>
        <video onClick={handleClick} ref={videoRef} className="video-player" src={url} />
        {renderControls({handlePlay, handleStop})}
        </>
};

VideoPlayer.propTypes = {
    url: PropTypes.string.isRequired,
    renderControls: PropTypes.func
}