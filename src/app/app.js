import React from 'react';
import {Root} from './root';
import {AppComponent} from './app-component';

export const App = () => {
  return (
    <Root>
      <AppComponent />
    </Root>
  );
};
