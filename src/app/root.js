import React from 'react';
import {Provider} from 'react-redux';
import {getStore} from './store';

const store = getStore();

export const Root = ({children}) => {
  return (
    <Provider store={store}>
        {children}
    </Provider>
  );
};
