import React from 'react';
import {Home} from "../pages/home";
import {Footer, Header} from "../ui-kit";

export const AppComponent = () => {
  return (
    <>
    <Header />
    <Home />
    <Footer />
    </>
  );
};
