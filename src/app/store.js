import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import {getReducers} from '../core/reducers';

const initialState = {};

const enhancerList = [];
// eslint-disable-next-line no-underscore-dangle
const devToolsExtension = window && window.__REDUX_DEVTOOLS_EXTENSION__;

if (typeof devToolsExtension === 'function') {
  enhancerList.push(devToolsExtension());
}

export const getStore = () => {
  const middlewares = [];

  const store = createStore(
    combineReducers(getReducers()),
    initialState,
    compose(applyMiddleware(...middlewares), ...enhancerList)
  );
  return store;
};
