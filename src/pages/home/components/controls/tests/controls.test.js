import {shallow} from 'enzyme';
import {Controls} from "../controls";

describe('Controls',() => {
    it('should render successfully', function () {
        const wrap = shallow(<Controls onPlay={jest.fn()} onStop={jest.fn()}  />);

        expect(wrap).toMatchSnapshot();
    });
    describe('behavior', function () {
        describe('when click on play button', function () {
            it('should call onPlay', function () {
                const onPlay = jest.fn();
                const wrap = shallow(<Controls onPlay={onPlay} onStop={jest.fn()}  />);
                wrap.find('Button').first().simulate('click');
                expect(onPlay).toHaveBeenCalled();
            });
        });
        describe('when click on stop button', function () {
            it('should call onStop', function () {
                const onStop = jest.fn();
                const wrap = shallow(<Controls onPlay={jest.fn()} onStop={onStop}  />);
                wrap.find('Button').last().simulate('click');
                expect(onStop).toHaveBeenCalled();
            });
        });
    });
})