import {Button, BUTTON_THEME} from "../../../../ui-kit";
import './controls.scss';
import PropTypes from "prop-types";

export const Controls = ({onPlay, onStop}) => {
    return <div className="controls">
        <div className="controls__button">
            <Button name="Play" onClick={onPlay} theme={BUTTON_THEME.green}/>
        </div>
        <div className="controls__button">
            <Button name="Stop" onClick={onStop} theme={BUTTON_THEME.red}/>
        </div>
    </div>
}

Controls.propTypes = {
    onPlay: PropTypes.func,
    onStop: PropTypes.func
}