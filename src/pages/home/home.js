import {useDispatch, useSelector} from "react-redux";
import {videoLoaded} from "./actions";
import {videoNameSelector, videoUrlSelector} from './selectors';
import {FILE_INPUT_EXTENSIONS, FileInput, VideoPlayer} from "../../ui-kit";
import {Controls} from "./components";
import {getVideoName} from "./helpers";

import './home.scss';

export const Home = () => {

    const dispatch = useDispatch();
    const videoUrl = useSelector(videoUrlSelector);
    const videoName = useSelector(videoNameSelector);
    const handleFileChange = event => {
        dispatch(videoLoaded(event.target.files[0]));
    };

    return <div className="home">
        {!videoName && <strong>Pick video file</strong>}
        <div className="home__input">
            <FileInput onChange={handleFileChange} accept={[FILE_INPUT_EXTENSIONS.video]}/>
        </div>
        {videoName &&
            <>
                <div className="home__video-name">Video name: {getVideoName(videoName)} </div>
                <VideoPlayer url={videoUrl} renderControls={({handlePlay, handleStop}) => (
                    <Controls onPlay={handlePlay} onStop={handleStop}/>
                )
                }/>
            </>
        }
    </div>
}