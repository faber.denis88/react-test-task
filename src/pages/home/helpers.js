export const getVideoName = (videoName='') => {
    if(typeof videoName !== 'string' ) {
        return ''
    }
    return videoName?.split('.')[0]
}