import {handleActions} from 'redux-actions';

import {videoLoaded} from './actions';

export const initialState = {
    videoUrl: null,
    videoName: ''
};

const reducerMap = {
    [videoLoaded]: (state, {payload}) => {
        return {
            ...state,
            videoUrl: URL.createObjectURL(payload),
            videoName: payload.name
        };
    }
};

export const homeReducer = handleActions(reducerMap, initialState);
