import {createSelector} from 'reselect';
import {HOME_PAGE_KEY} from './constants';

export const getHomePage = (state) => state[HOME_PAGE_KEY];

export const videoNameSelector = createSelector(getHomePage, (homepage) => homepage.videoName);
export const videoUrlSelector = createSelector(getHomePage, (homepage) => homepage.videoUrl);
