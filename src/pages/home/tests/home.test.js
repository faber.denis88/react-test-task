import {shallow} from "enzyme";
import {Home} from "../home";
import {useSelector} from "react-redux";
import {videoLoaded} from "../actions";

const mockDispatch = jest.fn();
jest.mock('react-redux', () => ({
    useSelector: jest.fn(),
    useDispatch: () => mockDispatch
}));

describe('Home', function () {
    describe('view', function () {
        describe('when video loaded', function () {
            it('should render successfully', function () {
                useSelector.mockReturnValue('src');
                useSelector.mockReturnValue('name');
                const wrap = shallow(<Home />);

                expect(wrap).toMatchSnapshot();
            });
        });
        describe('when video not loaded', function () {
            it('should not render VideoPlayer', function () {
                useSelector.mockReturnValue(null);
                useSelector.mockReturnValue('');
                const wrap = shallow(<Home />);
                const strongEl = wrap.find('strong');
                const VideoPlayerEl = wrap.find('VideoPlayer');
                expect(strongEl).toHaveLength(1);
                expect(VideoPlayerEl).toHaveLength(0);
            });
        });
    });
    describe('behavior', function () {
        describe('when file loaded', function () {
            it('should dispatch videoLoaded action', function () {
                useSelector.mockReturnValue('src');
                useSelector.mockReturnValue('name');
                const wrap = shallow(<Home />);
                const event = {
                    target: {
                        files: [
                            'dummyValue.something'
                        ]
                    }
                }
                wrap.find('FileInput').simulate('change', event)
                expect(mockDispatch).toHaveBeenCalledWith(videoLoaded(event.target.files[0]))
            });
        });
    });
});