import {videoUrlSelector, videoNameSelector, getHomePage} from '../selectors';
import {HOME_PAGE_KEY} from "../constants";


describe('home page selectors', () => {
    describe('getHomePage', () => {
        it('should return cart page data from store', () => {
            const store = {
                [HOME_PAGE_KEY]: 'store'
            };
            expect(getHomePage(store)).toEqual(store[HOME_PAGE_KEY]);
        });
    });
    describe('videoUrlSelector', () => {
        it('should return videoUrl from store', () => {
            const homePage = {videoName: "name", videoUrl: {}}
            expect(videoUrlSelector.resultFunc(homePage)).toEqual(homePage.videoUrl);
        });
    });
    describe('videoNameSelector', () => {
        it('should return videoName from store', () => {
            const homePage = {videoName: "name", videoUrl: {}}
            expect(videoNameSelector.resultFunc(homePage)).toEqual(homePage.videoName);
        });
    });
});