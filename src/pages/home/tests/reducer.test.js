import {createStore} from 'redux';
import {homeReducer, initialState} from '../reducer';
import {
    videoLoaded
} from '../actions';

describe('homeReducer', () => {
    let store;
    global.URL.createObjectURL = jest.fn(() => ({}));
    beforeEach(() => {
        store = createStore(homeReducer);
    });

    it('is expected to have initial state', () => {
        expect(store.getState()).toEqual(initialState);
    });

    it('is expected to handle videoLoaded action', () => {
        store.dispatch(videoLoaded({name: 'name'}));
        const expected = {videoName: "name", videoUrl: {}};
        expect(store.getState()).toEqual(expected);
    });

});
