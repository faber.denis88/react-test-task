import {getVideoName} from '../helpers';

describe('helpers', function () {
    describe('getVideoName', function () {
        describe('when passed string with . separator', function () {
            it('should return first part of string', function () {
                const firstPart = 'first';
                const secondPart = 'second';
                expect(getVideoName(`${firstPart}.${secondPart}`)).toEqual(firstPart)
            });
        });
        describe('when passed string without separator', function () {
            it('should return string as is', function () {
                const string = 'string';

                expect(getVideoName(string)).toEqual(string)
            });
        });
        describe('when called with not string argument', function () {
            it('should return empty string', function () {

                expect(getVideoName([])).toEqual('')
            });
        });
        describe('when called without argument', function () {
            it('should return empty string', function () {

                expect(getVideoName()).toEqual('')
            });
        });
    });
});