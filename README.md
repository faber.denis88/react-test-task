# Test task

## Lets image there is no video players. You are the last React engineer and in order to save the world you will need to create a video player.

What this video player need?

1. Button to play video
2. Input to upload video so it could be played
3. Button to stop playing video
4. Video on the screen

## Evaluation criteria

- State management approach
- Code readability
- Testing approach
- Component hierarchy and structure

## How to provide results

- Please send the forked repository id
- You can ask as much clarification questions as needed
- You can assume as much as you can, but make sure that what you need to ship
- You have to provide a code for this test task
